package com.infmaximus.voicenewsreader.ui.newslist.adapter

import android.annotation.SuppressLint
import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.daimajia.swipe.SwipeLayout
import com.facebook.drawee.view.SimpleDraweeView
import com.infmaximus.voicenewsreader.R
import com.infmaximus.voicenewsreader.data.models.News
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat

class NewsListAdapter : RecyclerView.Adapter<ViewHolder>() {

    @SuppressLint("SimpleDateFormat")
    private val sdf = SimpleDateFormat("HH:mm yyyy-MM-dd")
    private var news = mutableListOf<News>()

    //ToDo Для очистки повторного сигнала свайпа.
    private var lastOpenPosition = -1

    var setInteractListener: ((InteractType, Int) -> Unit)? = null

    fun setArrayList(arrayList: List<News>) {
        this.news = arrayList.toMutableList()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_news,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.tvTitle.text = news[position].title
        holder.tvDate.text = sdf.format(news[position].date)


        //Добавляем лисенер на открытие списка, для проигровки текста, по скролу
        holder.swipeLayout.addSwipeListener(object : SwipeLayout.SwipeListener {

            override fun onStartOpen(layout: SwipeLayout) {
            }

            override fun onOpen(layout: SwipeLayout) {
                if(!checkAndClearRepeatSwipe(holder.adapterPosition)) {
                    setInteractListener?.invoke(InteractType.SWIPE, holder.adapterPosition)
                    GlobalScope.launch(Dispatchers.Main) {
                        delay(100)
                        holder.swipeLayout.close()
                    }
                }
            }

            override fun onStartClose(layout: SwipeLayout) {}
            override fun onClose(layout: SwipeLayout) {}
            override fun onUpdate(layout: SwipeLayout, leftOffset: Int, topOffset: Int) {}
            override fun onHandRelease(layout: SwipeLayout, xvel: Float, yvel: Float) {}

        })

        holder.frescoView.setOnClickListener {
            setInteractListener?.invoke(InteractType.CLICK, position)
        }

        val urlStr = news[position].imageLinc
        val url = Uri.parse(urlStr)
            .buildUpon()
            .build()
            .toString()

        holder.frescoView.setImageURI(url)
    }

    override fun getItemCount(): Int {
        return news.size
    }

    private fun checkAndClearRepeatSwipe(newOpenPosition: Int): Boolean {
        return if(newOpenPosition == lastOpenPosition){
            lastOpenPosition = -1
            true
        } else {
            lastOpenPosition = newOpenPosition
            false
        }
    }

}

class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    var tvTitle: TextView = itemView.findViewById<View>(R.id.tvTitle) as TextView
    var tvDate: TextView = itemView.findViewById<View>(R.id.tvDateNew) as TextView
    var frescoView: SimpleDraweeView =
        itemView.findViewById<View>(R.id.frescoView) as SimpleDraweeView
    var swipeLayout: SwipeLayout = itemView.findViewById<View>(R.id.swipeLayout) as SwipeLayout

}

enum class InteractType {
    CLICK,
    SWIPE
}