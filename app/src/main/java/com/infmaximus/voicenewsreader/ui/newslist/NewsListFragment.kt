package com.infmaximus.voicenewsreader.ui.newslist

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.infmaximus.voicenewsreader.R
import com.infmaximus.voicenewsreader.service.Action
import com.infmaximus.voicenewsreader.service.Action.Companion.READ_FULL_TEXT
import com.infmaximus.voicenewsreader.service.Action.Companion.READ_SHORT_TEXT
import com.infmaximus.voicenewsreader.service.ReaderService
import com.infmaximus.voicenewsreader.ui.newslist.adapter.InteractType
import com.infmaximus.voicenewsreader.ui.newslist.adapter.NewsListAdapter
import kotlinx.android.synthetic.main.fragment_list_news.view.*
import org.koin.android.viewmodel.ext.android.getViewModel

class NewsListFragment : Fragment() {

    @SuppressLint("InflateParams")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_list_news, null)
        val context: Context = this.context ?: return view

        val viewModel = getViewModel<NewsListViewModel>()

        val newsListAdapter = NewsListAdapter()
        newsListAdapter.setInteractListener = { interactType: InteractType, position: Int ->
            viewModel.interactNews(interactType, position)
        }

        view.recyclerViewNews.adapter = newsListAdapter
        view.recyclerViewNews.layoutManager = LinearLayoutManager(context)
        view.recyclerViewNews.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (!recyclerView.canScrollVertically(1)) {
                    viewModel.loadNewsList()
                }
            }
        })

        val swipeRefreshLayout = view.findViewById(R.id.swipeContainer) as SwipeRefreshLayout
        swipeRefreshLayout.setOnRefreshListener {
            viewModel.updateNews()
        }

        viewModel.newsList.observe(this, Observer {
            if (it != null) {
                newsListAdapter.setArrayList(it)
                newsListAdapter.notifyDataSetChanged()

                if(it.isEmpty()){
                    view.emptyText.visibility = View.VISIBLE
                } else {
                    view.emptyText.visibility = View.GONE
                }
            }
        })

        viewModel.urlForFullText.observe(this, Observer {
            val startIntent = Intent(requireActivity(), ReaderService::class.java)
            startIntent.putExtra(READ_FULL_TEXT, it)
            startService(startIntent)
        })

        viewModel.textForRead.observe(this, Observer { unsafeTextWithUrl ->
            unsafeTextWithUrl?.let {
                val startIntent = Intent(requireActivity(), ReaderService::class.java)
                startIntent.putExtra(READ_SHORT_TEXT, it.text)
                startIntent.putExtra(READ_FULL_TEXT, it.url)
                startService(startIntent)
            }
        })

        viewModel.loading.observe(this, Observer {
            swipeRefreshLayout.isRefreshing = it == true
        })

        return view
    }

    private fun startService(startIntent: Intent) {
        startIntent.action = Action.START_FOREGROUND_ACTION
        requireActivity().startService(startIntent)
    }

}