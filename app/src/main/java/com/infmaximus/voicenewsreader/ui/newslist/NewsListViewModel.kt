package com.infmaximus.voicenewsreader.ui.newslist

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.infmaximus.voicenewsreader.data.models.News
import com.infmaximus.voicenewsreader.domain.interactor.INewsListInteractor
import com.infmaximus.voicenewsreader.ui.livedata.SingleLiveEvent
import com.infmaximus.voicenewsreader.domain.models.TextWithUrl
import com.infmaximus.voicenewsreader.ui.newslist.adapter.InteractType

class NewsListViewModel(private val newsListInteractor: INewsListInteractor) : ViewModel() {

    val loading: MutableLiveData<Boolean> by lazy {
        MutableLiveData<Boolean>()
    }

    val newsList: MutableLiveData<List<News>> by lazy {
        updateNews()
        MutableLiveData<List<News>>()
    }

    val textForRead: SingleLiveEvent<TextWithUrl> by lazy {
        SingleLiveEvent<TextWithUrl>()
    }

    val urlForFullText: SingleLiveEvent<String> by lazy {
        SingleLiveEvent<String>()
    }

    fun updateNews() {
        loading.value = true
        newsListInteractor.updateNews {
            loading.postValue(false)

            newsList.postValue(it)
        }
    }

    fun loadNewsList() {
        loading.value = true
        newsListInteractor.loadNews {
            loading.postValue(false)

            newsList.postValue(it)
        }
    }

    fun interactNews(interactType: InteractType, position: Int) {
        when (interactType) {
            InteractType.CLICK -> {
                //ToDo функционал под вопросом.
                //textForRead.value = newsListInteractor.readIntroduction(position)
            }

            InteractType.SWIPE -> urlForFullText.value = newsListInteractor.readFullText(position)
        }
    }

}