package com.infmaximus.voicenewsreader.ui.control

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.infmaximus.voicenewsreader.domain.interactor.ControlOnClick
import com.infmaximus.voicenewsreader.domain.interactor.IControlInteractor
import com.infmaximus.voicenewsreader.domain.models.PreviewNews
import io.reactivex.disposables.CompositeDisposable

class SpeechControlViewModel(private val controlInterator: IControlInteractor) : ViewModel() {

    private var currentControlState = ControlMode.PLAY

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    val newsForControl: MutableLiveData<PreviewNews> by lazy {
        MutableLiveData<PreviewNews>()
    }

    val stateControl: MutableLiveData<ControlMode> by lazy {
        MutableLiveData<ControlMode>()
    }

    val loading: MutableLiveData<Boolean> by lazy {
        MutableLiveData<Boolean>()
    }

    init {
        loading.value = true

        compositeDisposable.add(controlInterator.previewContent
            .subscribe {
                newsForControl.postValue(it)
            }
        )

        compositeDisposable.add(controlInterator.state
            .subscribe {
                loading.postValue(false)

                when (it) {
                    ControlOnClick.PLAY -> currentControlState = ControlMode.PLAY

                    ControlOnClick.PAUSE -> currentControlState = ControlMode.PAUSE

                    else -> {
                    }
                }

                stateControl.postValue(currentControlState)
            }
        )
    }

    fun onStopClick() {
        controlInterator.stop()
    }

    fun onPlayClick() {
        when (currentControlState) {
            ControlMode.PLAY -> controlInterator.pause()

            ControlMode.PAUSE -> controlInterator.play()
        }
    }

    fun onDestroy() {
        compositeDisposable.clear()
    }

}

enum class ControlMode {
    PLAY,
    PAUSE
}