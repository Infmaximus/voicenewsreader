package com.infmaximus.voicenewsreader.ui.control

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.infmaximus.voicenewsreader.R
import kotlinx.android.synthetic.main.fragment_speech_control.view.*
import org.koin.android.viewmodel.ext.android.getViewModel

class SpeechControlFragment : Fragment() {

    private lateinit var viewModel: SpeechControlViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        retainInstance = true
    }

    @SuppressLint("InflateParams")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_speech_control, null)

        val context = context ?: return view

        viewModel = getViewModel()

        viewModel.newsForControl.observe(this, Observer { unsafePreviewNews ->
            unsafePreviewNews?.let {
                view.tvTitle.text = it.title

                val urlStr = it.imageUrl
                val url = Uri.parse(urlStr)
                    .buildUpon()
                    .build()
                    .toString()

                view.frescoView.setImageURI(url)
            }
        })

        viewModel.stateControl.observe(this, Observer { unsafeControlMode ->
            unsafeControlMode?.let {
                view.ivControlPlay.setImageDrawable(
                    ContextCompat.getDrawable(
                        context,
                        when (it) {

                            ControlMode.PLAY -> R.drawable.ic_pause_black_24dp
                            ControlMode.PAUSE -> R.drawable.ic_play_arrow_black_24dp
                        }
                    )
                )
            }
        })

        viewModel.loading.observe(this, Observer { unsafeLoadingState ->
            if (unsafeLoadingState == true) {
                view.progressBar.visibility = View.VISIBLE
            } else {
                view.progressBar.visibility = View.GONE
            }
        })

        view.ivStop.setOnClickListener {
            viewModel.onStopClick()
        }

        view.ivControlPlay.setOnClickListener {
            viewModel.onPlayClick()
        }

        return view
    }

    override fun onDestroy() {
        super.onDestroy()

        viewModel.onDestroy()
    }

}