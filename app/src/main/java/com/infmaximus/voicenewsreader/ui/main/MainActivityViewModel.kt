package com.infmaximus.voicenewsreader.ui.main

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.util.Log
import com.infmaximus.voicenewsreader.domain.interactor.ControlOnClick
import com.infmaximus.voicenewsreader.domain.interactor.IStateControlInteractor
import io.reactivex.disposables.Disposable

class MainActivityViewModel(private val controlInterator: IStateControlInteractor) : ViewModel() {

    private var disposable: Disposable? = null

    val stateBehavior: MutableLiveData<ControlState> = MutableLiveData()

    fun initViewModel() {
        disposable = controlInterator
            .state
            .subscribe ({
                when (it) {
                    ControlOnClick.STOP -> stateBehavior.postValue(ControlState.HIDE)

                    ControlOnClick.DEFAULT -> {
                    }

                    else -> stateBehavior.postValue(ControlState.SHOW)
                }
            },{
                Log.d("LOG_TAG_ERROR", "controlInterator state error")
            })
    }

    fun onDestroy() {
        disposable?.dispose()
    }

}

enum class ControlState {
    SHOW,
    HIDE
}