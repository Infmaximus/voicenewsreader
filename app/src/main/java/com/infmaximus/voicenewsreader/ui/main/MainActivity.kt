package com.infmaximus.voicenewsreader.ui.main

import android.arch.lifecycle.Observer
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.infmaximus.voicenewsreader.R
import android.support.design.widget.BottomSheetBehavior
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.viewmodel.ext.android.getViewModel


class MainActivity : AppCompatActivity() {

    private lateinit var viewModel: MainActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val bottomSheetBehavior = BottomSheetBehavior.from(constraintBehavior)
        bottomSheetBehavior.isHideable = true
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN

        viewModel = getViewModel()

        viewModel.stateBehavior.observe(this, Observer {unsafeControlState ->
            unsafeControlState?.let {
                when(it){

                    ControlState.SHOW -> bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED)

                    ControlState.HIDE -> bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN)

                }
            }
        })

        viewModel.initViewModel()
    }

    override fun onDestroy() {
        super.onDestroy()

        viewModel.onDestroy()
    }

}
