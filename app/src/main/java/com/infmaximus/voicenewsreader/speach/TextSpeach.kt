package com.infmaximus.voicenewsreader.speach

import android.content.Context
import android.speech.tts.TextToSpeech
import android.speech.tts.UtteranceProgressListener
import java.util.*

class TextSpeaker(mContext: Context) {

    private lateinit var textToSpeech: TextToSpeech

    var endRead: (() -> Unit)? = null

    init {
        val myLocale = Locale("ru", "RU")
        textToSpeech = TextToSpeech(mContext) { status ->
            if (status != TextToSpeech.ERROR) {
                textToSpeech.language = myLocale
            }
        }

        textToSpeech.setOnUtteranceProgressListener(object : UtteranceProgressListener() {
            override fun onError(p0: String?) {
            }

            override fun onStart(s: String) {
            }

            override fun onDone(s: String) {
                endRead?.invoke()
            }

        })
    }


    fun listenText(text: String) {
        val utteranceId = this.hashCode().toString() + ""
        textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, null, utteranceId)
    }

    fun stopVoice() {
        textToSpeech.stop()
    }

}