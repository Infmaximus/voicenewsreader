package com.infmaximus.voicenewsreader

import android.app.Application
import com.facebook.drawee.backends.pipeline.Fresco
import com.infmaximus.voicenewsreader.data.repositories.*
import com.infmaximus.voicenewsreader.domain.interactor.*
import com.infmaximus.voicenewsreader.domain.repository.IBitmapLoader
import com.infmaximus.voicenewsreader.domain.repository.INewsListProviderRepository
import com.infmaximus.voicenewsreader.domain.repository.INewsLoaderRepository
import com.infmaximus.voicenewsreader.domain.repository.INewsProvider
import com.infmaximus.voicenewsreader.service.IReaderViewModel
import com.infmaximus.voicenewsreader.service.ReaderViewModel
import com.infmaximus.voicenewsreader.ui.control.SpeechControlViewModel
import com.infmaximus.voicenewsreader.ui.main.MainActivityViewModel
import com.infmaximus.voicenewsreader.ui.newslist.NewsListViewModel
import org.koin.android.ext.android.startKoin
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

class VNRApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        Fresco.initialize(this)
        startKoin(
            this,
            listOf(
                voiceNewsReader
            )
        )
    }

}

// declared ViewModel using the viewModel keyword
val voiceNewsReader = module {
    viewModel { NewsListViewModel(get()) }
    single { NewsListInteractor(get()) as INewsListInteractor }
    single { FourPDANewsListLoaderRepository() as INewsListProviderRepository } bind INewsProvider::class

    viewModel { MainActivityViewModel(get()) }
    viewModel { SpeechControlViewModel(get()) }

    single { BitmapLoader() as IBitmapLoader }
    single { ControlInterator() as IControlInteractor } bind IStateControlInteractor::class
    single { ReaderViewModel(get(), get()) as IReaderViewModel }
    single { NewsLoaderRepository() as INewsLoaderRepository }
    single { ServiceInteractor(get(), get(), get()) as IServiceInteractor }
}