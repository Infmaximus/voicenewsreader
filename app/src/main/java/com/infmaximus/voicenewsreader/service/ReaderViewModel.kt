package com.infmaximus.voicenewsreader.service

import android.arch.lifecycle.MutableLiveData
import android.graphics.Bitmap
import com.infmaximus.voicenewsreader.domain.interactor.*
import com.infmaximus.voicenewsreader.domain.models.PreviewNews
import com.infmaximus.voicenewsreader.domain.models.TextWithUrl
import com.infmaximus.voicenewsreader.domain.repository.IBitmapLoader
import com.infmaximus.voicenewsreader.ui.livedata.SingleLiveEvent
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

interface IReaderViewModel {

    var textForReadingClosure: ((String) -> Unit)?

    var controlClosure: ((ControlOnClick) -> Unit)?

    val textForReading: MutableLiveData<String>

    val control: MutableLiveData<ControlOnClick>

    val previewWithControl: SingleLiveEvent<PreviewWithControl>

    fun initDisposable()

    fun readByText(textWithUrl: TextWithUrl)

    fun readByUrl(url: String)

    fun endOfRead()

    fun previous()

    fun play()

    fun stop()

    fun next()

}

class ReaderViewModel(
    private val bitmapLoader: IBitmapLoader,
    private val serviceInteractor: IServiceInteractor
) : IReaderViewModel {

    private var newsImage: Bitmap? = null
    private var previewNews: PreviewNews = PreviewNews()
    private var controlOnClick: ControlOnClick = ControlOnClick.DEFAULT

    private lateinit var stateDisposable: Disposable
    private lateinit var textDisposable: Disposable
    private lateinit var previewContentDisposable: Disposable

    override var textForReadingClosure: ((String) -> Unit)? = null

    override var controlClosure: ((ControlOnClick) -> Unit)? = null

    override val control: MutableLiveData<ControlOnClick> by lazy {
        MutableLiveData<ControlOnClick>()
    }

    override val previewWithControl: SingleLiveEvent<PreviewWithControl> by lazy {
        SingleLiveEvent<PreviewWithControl>()
    }

    override val textForReading = MutableLiveData<String>()

    override fun initDisposable() {
        stateDisposable = serviceInteractor.state
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                controlOnClick = it

                previewWithControl.postValue(
                    PreviewWithControl(
                        previewNews,
                        controlOnClick,
                        newsImage
                    )
                )

                if (it == ControlOnClick.STOP) {
                    stateDisposable.dispose()
                    textDisposable.dispose()
                }
            }

        textDisposable = serviceInteractor.textForSpeech
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                textForReading.postValue(it)
                textForReadingClosure?.invoke(it)
            }

        previewContentDisposable = serviceInteractor.previewContent
            .observeOn(Schedulers.io())
            .subscribe {
                previewNews = it

                newsImage = bitmapLoader.getBitmapFromURL(previewNews.imageUrl)

                previewWithControl.postValue(
                    PreviewWithControl(
                        previewNews,
                        controlOnClick,
                        newsImage
                    )
                )
            }

    }

    override fun readByText(textWithUrl: TextWithUrl) {
        serviceInteractor.readText(textWithUrl)
    }

    override fun readByUrl(url: String) {
        serviceInteractor.loadNews(url)
    }

    override fun endOfRead() {
        serviceInteractor.endOfRead()
    }

    override fun previous() {
        serviceInteractor.previous()
    }

    override fun play() {
        serviceInteractor.play()
    }

    override fun stop() {
        serviceInteractor.stop()
    }

    override fun next() {
        serviceInteractor.next()
    }

}

data class PreviewWithControl(
    val previewNews: PreviewNews,
    val controlOnClick: ControlOnClick,
    val newsImage: Bitmap? = null
)