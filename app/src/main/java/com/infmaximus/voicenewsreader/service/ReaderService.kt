package com.infmaximus.voicenewsreader.service

import android.app.*
import android.content.Intent
import android.os.IBinder
import android.support.v4.app.NotificationCompat
import android.graphics.BitmapFactory
import android.arch.lifecycle.LifecycleService
import android.arch.lifecycle.Observer
import android.content.Context
import android.graphics.Bitmap
import android.os.Build
import com.infmaximus.voicenewsreader.domain.interactor.ControlOnClick
import com.infmaximus.voicenewsreader.service.Action.Companion.READ_FULL_TEXT
import com.infmaximus.voicenewsreader.service.Action.Companion.READ_SHORT_TEXT
import com.infmaximus.voicenewsreader.speach.TextSpeaker
import org.koin.android.ext.android.inject
import android.widget.RemoteViews
import com.infmaximus.voicenewsreader.R
import com.infmaximus.voicenewsreader.domain.models.PreviewNews
import android.app.NotificationChannel
import android.app.NotificationManager
import com.infmaximus.voicenewsreader.domain.models.TextWithUrl
import com.infmaximus.voicenewsreader.ui.main.MainActivity

class ReaderService : LifecycleService() {

    companion object {
        const val FOREGROUND_SERVICE = 118
    }

    lateinit var textSpeaker: TextSpeaker

    private val readerViewModel: IReaderViewModel by inject()

    override fun onCreate() {
        super.onCreate()

        readerViewModel.initDisposable()

        textSpeaker = TextSpeaker(this)

        textSpeaker.endRead = {
            readerViewModel.endOfRead()
        }

        readerViewModel.textForReading.observe(this, Observer {
            textSpeaker.listenText(it ?: "")
        })

        readerViewModel.previewWithControl.observe(this, Observer { unsafePreviewWithControl ->
            unsafePreviewWithControl?.let {
                updateService(it.controlOnClick, it.previewNews, it.newsImage)
            }
        })
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)
        when {
            intent.action == Action.START_FOREGROUND_ACTION -> {
                val notification = getNotification(true)

                intent.extras?.let { bundle ->
                    when {
                        bundle.containsKey(READ_SHORT_TEXT) -> {
                            readerViewModel.readByText(
                                TextWithUrl(
                                    bundle.getString(READ_SHORT_TEXT) ?: "",
                                    bundle.getString(READ_FULL_TEXT) ?: ""
                                )
                            )
                        }

                        bundle.containsKey(READ_FULL_TEXT) -> {
                            readerViewModel.readByUrl(bundle.getString(READ_FULL_TEXT) ?: "")
                        }
                    }
                }

                startForeground(
                    FOREGROUND_SERVICE,
                    notification
                )
            }

            intent.action == Action.STOP_ACTION -> {
                readerViewModel.stop()
            }

            intent.action == Action.PLAY_ACTION -> {
                readerViewModel.play()
            }

            intent.action == Action.STOP_FOREGROUND_ACTION -> {
                stopWork()
            }
        }

        return Service.START_STICKY
    }

    override fun onBind(p0: Intent?): IBinder? {
        super.onBind(p0)
        return null
    }

    private fun getNotification(
        play: Boolean,
        previewNews: PreviewNews = PreviewNews(),
        newsImage: Bitmap? = null
    ): Notification {

        val notificationIntent = Intent(this, MainActivity::class.java)
        notificationIntent.action = Intent.ACTION_VIEW
        notificationIntent.addFlags(
            Intent.FLAG_ACTIVITY_NEW_TASK or
                    Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
        )

        val showActivityIntent = PendingIntent.getActivity(
            this, 0,
            notificationIntent, 0
        )

        val stopIntent = Intent(this, ReaderService::class.java)
        stopIntent.action = Action.STOP_ACTION
        val restartStopIntent = PendingIntent.getService(this, 0, stopIntent, 0)

        val playIntent = Intent(this, ReaderService::class.java)
        playIntent.action = Action.PLAY_ACTION
        val restartPlayIntent = PendingIntent.getService(this, 0, playIntent, 0)

        val remoteViews = RemoteViews(packageName, R.layout.notification_simple)

        remoteViews.setTextViewText(R.id.tvTitle, previewNews.title)
        remoteViews.setOnClickPendingIntent(R.id.root, showActivityIntent)

        remoteViews.setOnClickPendingIntent(R.id.ivControlPlay, restartPlayIntent)
        remoteViews.setOnClickPendingIntent(R.id.ivStop, restartStopIntent)

        remoteViews.setImageViewResource(R.id.ivStop, R.drawable.ic_stop_black_24dp)

        remoteViews.setImageViewResource(
            R.id.ivControlPlay, if (play) {
                R.drawable.ic_pause_black_24dp
            } else {
                R.drawable.ic_play_arrow_black_24dp
            }
        )

        val icon = newsImage ?: BitmapFactory.decodeResource(
            resources,
            R.drawable.news_title
        )

        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        val channelId = "newsChannel"

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = "VoiceNewsReader"
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(channelId, name, importance)

            channel.enableVibration(true)
            channel.vibrationPattern = longArrayOf(0)
            notificationManager.createNotificationChannel(channel)
        }

        val notification = NotificationCompat.Builder(this, channelId)
            .setSmallIcon(R.drawable.ic_volume_mute_black_24dp)
            .setLargeIcon(icon)
            .setContentIntent(showActivityIntent)
            .setCustomContentView(remoteViews)
            .setStyle(NotificationCompat.DecoratedCustomViewStyle())
            .setOngoing(true)
            .build()


        notificationManager.notify(FOREGROUND_SERVICE, notification)
        return notification
    }

    private fun stopReading() {
        textSpeaker.stopVoice()
    }

    private fun stopWork() {
        stopReading()

        stopForeground(true)
        stopSelf()
    }

    private fun updateService(
        controlOnClick: ControlOnClick,
        previewNews: PreviewNews,
        newsImage: Bitmap?
    ) {
        when (controlOnClick) {
            ControlOnClick.PLAY -> {
                getNotification(true, previewNews, newsImage)
            }

            ControlOnClick.PAUSE -> {
                getNotification(false, previewNews, newsImage)
                stopReading()
            }

            ControlOnClick.STOP -> {
                stopWork()
            }

            else -> {

            }
        }
    }

}

class Action {
    companion object {
        const val READ_SHORT_TEXT = "short_text"
        const val READ_FULL_TEXT = "full_text"
        const val PLAY_ACTION = "voice_play"
        const val STOP_ACTION = "voice_stop"
        const val START_FOREGROUND_ACTION = "start_foreground"
        const val STOP_FOREGROUND_ACTION = "stop_foreground"
    }
}