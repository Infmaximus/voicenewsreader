package com.infmaximus.voicenewsreader.domain.interactor

import com.infmaximus.voicenewsreader.data.models.News
import com.infmaximus.voicenewsreader.domain.models.TextWithUrl
import com.infmaximus.voicenewsreader.domain.repository.INewsListProviderRepository
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

interface INewsListInteractor {
    fun updateNews(newsList: (List<News>) -> Unit)

    fun loadNews(newsList: (List<News>) -> Unit)

    fun readIntroduction(position: Int): TextWithUrl

    fun readFullText(position: Int): String
}

class NewsListInteractor(private val newsListLoaderRepository: INewsListProviderRepository) :
    INewsListInteractor {

    var currentPage = 0

    override fun updateNews(newsList: (List<News>) -> Unit) {
        GlobalScope.launch {
            currentPage = 0
            newsListLoaderRepository.clearNews()
            newsListLoaderRepository.loadNewsList(currentPage, newsList)
        }
    }

    override fun loadNews(newsList: (List<News>) -> Unit) {
        GlobalScope.launch {
            currentPage++
            newsListLoaderRepository.loadNewsList(currentPage, newsList)
        }
    }

    override fun readIntroduction(position: Int) =
        TextWithUrl(
            newsListLoaderRepository.getIntroduction(position),
            newsListLoaderRepository.getLinkFullTextNews(position)
        )

    override fun readFullText(position: Int) =
        newsListLoaderRepository.getLinkFullTextNews(position)

}