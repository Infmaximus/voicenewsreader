package com.infmaximus.voicenewsreader.domain.repository

import android.graphics.Bitmap

interface IBitmapLoader{
    fun getBitmapFromURL(src: String): Bitmap?
}