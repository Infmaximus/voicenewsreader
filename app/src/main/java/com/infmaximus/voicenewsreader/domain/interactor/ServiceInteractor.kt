package com.infmaximus.voicenewsreader.domain.interactor

import com.infmaximus.voicenewsreader.domain.models.PreviewNews
import com.infmaximus.voicenewsreader.domain.models.TextWithUrl
import com.infmaximus.voicenewsreader.domain.repository.INewsLoaderRepository
import com.infmaximus.voicenewsreader.domain.repository.INewsProvider
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

interface IServiceInteractor {

    val state: Observable<ControlOnClick>

    val textForSpeech: PublishSubject<String>

    val previewContent: Observable<PreviewNews>

    fun loadNews(url: String)

    fun readText(textWithUrl: TextWithUrl)

    fun endOfRead()

    fun previous()

    fun play()

    fun stop()

    fun next()

}

class ServiceInteractor(
    val newsLoaderRepository: INewsLoaderRepository,
    val controlInterator: IControlInteractor,
    val newsProvider: INewsProvider
) : IServiceInteractor {

    private var modeRead = ReadMode.SENTENCE

    private var controlRead = ControlMode.PLAY

    var sentencePosition: Int = 0

    override val state: Observable<ControlOnClick> =
        controlInterator.state
            .doOnNext { controlOnClick ->
                if (controlOnClick == ControlOnClick.PLAY) {
                    controlRead = ControlMode.PLAY

                    GlobalScope.launch {
                        newsLoaderRepository.getPartOfTextByPosition(
                            sentencePosition,
                            {
                                textForSpeech.onNext(it)
                            },
                            {
                                controlInterator.stop()
                            })
                    }
                }

                if (controlOnClick == ControlOnClick.PAUSE) {
                    controlRead = ControlMode.PAUSE
                }
            }
            .flatMap { controlOnClick ->
                Observable.create<ControlOnClick> {
                    it.onNext(controlOnClick)
                }
            }

    override val textForSpeech: PublishSubject<String> = PublishSubject.create()

    override val previewContent = controlInterator.previewContent

    override fun loadNews(url: String) {
        sentencePosition = 0
        modeRead = ReadMode.SENTENCE

        controlInterator.previewContent.onNext(newsProvider.getNewsByUrl(url))

        GlobalScope.launch {
            newsLoaderRepository.loadNewsAndFetchFirstSentence(
                url,
                {
                    controlInterator.play()
                    textForSpeech.onNext(it)
                },
                {
                    controlInterator.stop()
                })
            }
    }

    override fun readText(textWithUrl: TextWithUrl) {
        GlobalScope.launch {
            controlInterator.play()
            modeRead = ReadMode.TEXT
            //textForSpeech.onNext(text)
        }
    }

    override fun endOfRead() {
        if (modeRead == ReadMode.SENTENCE) {
            sentencePosition++
            GlobalScope.launch {
                newsLoaderRepository.getPartOfTextByPosition(
                    sentencePosition,
                    {
                        textForSpeech.onNext(it)
                    },
                    {
                        controlInterator.stop()
                    })
            }
        }
    }

    override fun previous() {
        controlInterator.previous()
    }

    override fun play() {
        when (controlRead) {
            ControlMode.PLAY -> {
                controlInterator.pause()
            }

            ControlMode.PAUSE -> {
                controlInterator.play()
            }
        }
    }

    override fun stop() {
        controlInterator.stop()
    }

    override fun next() {
        controlInterator.next()
    }

    enum class ReadMode {
        TEXT,
        SENTENCE
    }

    enum class ControlMode {
        PLAY,
        PAUSE
    }
}