package com.infmaximus.voicenewsreader.domain.models

data class PreviewNews(val title: String = "", val imageUrl: String = "")