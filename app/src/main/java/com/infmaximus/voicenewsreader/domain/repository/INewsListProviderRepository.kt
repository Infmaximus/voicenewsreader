package com.infmaximus.voicenewsreader.domain.repository

import com.infmaximus.voicenewsreader.data.models.News
import com.infmaximus.voicenewsreader.domain.models.PreviewNews

interface INewsListLoaderRepository {

    fun clearNews()

    fun getIntroduction(position: Int): String

    fun getLinkFullTextNews(position: Int): String

    fun loadNewsList(part: Int, resultNewsList: (MutableList<News>) -> Unit)

}

interface INewsProvider {

    fun getNewsByUrl(url: String): PreviewNews

}

interface INewsListProviderRepository : INewsListLoaderRepository, INewsProvider