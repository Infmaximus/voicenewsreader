package com.infmaximus.voicenewsreader.domain.repository

interface INewsLoaderRepository {

    fun loadNewsAndFetchFirstSentence(
        url: String,
        sentence: (String) -> Unit,
        empty: () -> Unit
    )

    fun getPartOfTextByPosition(
        position: Int,
        sentence: (String) -> Unit,
        empty: () -> Unit
    )

}