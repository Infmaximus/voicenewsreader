package com.infmaximus.voicenewsreader.domain.interactor

import com.infmaximus.voicenewsreader.domain.models.PreviewNews
import io.reactivex.subjects.BehaviorSubject

interface IStateControlInteractor{

    val state: BehaviorSubject<ControlOnClick>

    fun play()
    fun pause()
    fun stop()
    fun next()
    fun previous()

}

interface IContentForControlInteractor{

    val previewContent: BehaviorSubject<PreviewNews>

}

interface IControlInteractor : IStateControlInteractor, IContentForControlInteractor

class ControlInterator: IControlInteractor{

    override val state = BehaviorSubject.create<ControlOnClick>()

    override val previewContent = BehaviorSubject.create<PreviewNews>()

    override fun play() {
        state.onNext(ControlOnClick.PLAY)
    }

    override fun pause() {
        state.onNext(ControlOnClick.PAUSE)
    }

    override fun stop() {
        state.onNext(ControlOnClick.STOP)
        state.onNext(ControlOnClick.DEFAULT)
    }

    override fun next() {
        state.onNext(ControlOnClick.NEXT)
    }

    override fun previous() {
        state.onNext(ControlOnClick.PREVIOUS)
    }

}

enum class ControlOnClick {
    PLAY,
    PAUSE,
    STOP,
    NEXT,
    PREVIOUS,
    DEFAULT
}