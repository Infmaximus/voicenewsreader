package com.infmaximus.voicenewsreader.domain.models

data class TextWithUrl(val text: String, val url: String)