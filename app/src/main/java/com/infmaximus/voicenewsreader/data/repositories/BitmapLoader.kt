package com.infmaximus.voicenewsreader.data.repositories

import android.graphics.BitmapFactory
import android.graphics.Bitmap
import com.infmaximus.voicenewsreader.domain.repository.IBitmapLoader
import java.io.IOException
import java.net.HttpURLConnection
import java.net.URL

class BitmapLoader : IBitmapLoader {
    override fun getBitmapFromURL(src: String): Bitmap? {
        return try {
            val url = URL(src)
            val connection = url.openConnection() as HttpURLConnection

            connection.doInput = true
            connection.connect()

            val input = connection.inputStream

            BitmapFactory.decodeStream(input)
        } catch (e: IOException) {
            null
        }
    }
}