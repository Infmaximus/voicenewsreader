package com.infmaximus.voicenewsreader.data.repositories

import android.annotation.SuppressLint
import android.util.Log
import com.infmaximus.voicenewsreader.data.models.News
import com.infmaximus.voicenewsreader.domain.models.PreviewNews
import com.infmaximus.voicenewsreader.domain.repository.INewsListProviderRepository
import com.squareup.okhttp.OkHttpClient
import com.squareup.okhttp.Request
import org.jsoup.Jsoup
import java.text.SimpleDateFormat

class FourPDANewsListLoaderRepository : INewsListProviderRepository {

    private var collectionArrayOfPartNews = HashMap<Int, MutableList<News>>()
    private var allNews = mutableListOf<News>()

    @SuppressLint("SimpleDateFormat")
    var sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")

    override fun clearNews() {
        collectionArrayOfPartNews.clear()
    }

    override fun loadNewsList(part: Int, resultNewsList: (MutableList<News>) -> Unit) {
        collectionArrayOfPartNews[part] = requestNewsListByPart(correctIndexPage(part))

        allNews.clear()

        collectionArrayOfPartNews.toSortedMap().forEach {
            allNews.addAll(it.value)
        }

        resultNewsList.invoke(allNews)
    }

    override fun getIntroduction(position: Int): String {
        return if (allNews.size > position) {
            allNews[position].shortText
        } else {
            ""
        }
    }

    override fun getLinkFullTextNews(position: Int): String {
        return if (allNews.size > position) {
            allNews[position].articleLink
        } else {
            ""
        }
    }

    override fun getNewsByUrl(url: String): PreviewNews {
        val article = allNews
            .firstOrNull {
                it.articleLink == url
            }

        return article?.let {
            PreviewNews(article.title, article.imageLinc ?: "")
        } ?: PreviewNews()
    }

    private fun correctIndexPage(part: Int) = part + 1

    private fun requestNewsListByPart(part: Int): MutableList<News> {
        val client = OkHttpClient()
        var newsList = mutableListOf<News>()
        val request = Request.Builder()
            .url("http://4pda.ru/page/$part/")
            .build()
        try {
            val response = client.newCall(request).execute()
            newsList = parsePageForNews(response.body()!!.string())
        } catch (ex: Exception) {
            Log.e("LOG_TAG_ERROR", "load 4pda error " + ex.toString())
        }

        return newsList
    }

    private fun parsePageForNews(allPage: String): MutableList<News> {
        return (Jsoup.parse(allPage).select("article[class=post]").mapNotNull { d ->
            try {
                //Парсим изображение
                val linkImage = "http:${d.select("img").attr("src")}"
                val elements = d.select("div[class=description]").first()
                val stringBuilder = d.select("meta[itemprop=datePublished]")
                    .attr("content")
                    .substring(
                        0, d.select("meta[itemprop=datePublished]")
                            .attr("content").indexOf('+')
                    )
                    .replace("T", " ")
                val dateInDate = sdf.parse(stringBuilder)

                News(
                    elements.select("a").attr("title"),
                    "",
                    elements.select("p").first().text(),
                    "http:" + elements.select("a").attr("href"),
                    dateInDate,
                    linkImage
                )
            } catch (e: Exception) {
                e.printStackTrace()
                null
            }
        }).toMutableList()
    }

}