package com.infmaximus.voicenewsreader.data.repositories

import android.util.Log
import com.infmaximus.voicenewsreader.domain.repository.INewsLoaderRepository
import com.squareup.okhttp.OkHttpClient
import com.squareup.okhttp.Request
import org.jsoup.Jsoup

class NewsLoaderRepository : INewsLoaderRepository {

    private var sentences = mutableListOf<String>()

    override fun loadNewsAndFetchFirstSentence(
        url: String,
        sentence: (String) -> Unit,
        empty: () -> Unit
    ) {
        sentences = loadSentences(url)

        if (sentences.isNotEmpty()) {
            sentence.invoke(sentences[0])
        } else {
            empty.invoke()
        }
    }

    override fun getPartOfTextByPosition(
        position: Int,
        sentence: (String) -> Unit,
        empty: () -> Unit
    ) {
        if (sentences.isNotEmpty() && sentences.size > position) {
            sentence.invoke(sentences[position])
        } else {
            empty.invoke()
        }
    }

    private fun loadSentences(url: String): MutableList<String> {
        val client = OkHttpClient()

        val request = Request.Builder()
            .url(url)
            .build()

        return try {
            val response = client.newCall(request).execute()
            val fullText = parsFullTextFromHtml(response.body().string())
            splitTextToSentences(fullText)

        } catch (ex: Exception) {
            Log.e("LOG_TAG_ERROR", "load page 4pda error " + ex.toString())
            mutableListOf()
        }
    }

    private fun parsFullTextFromHtml(page: String) =
        Jsoup.parse(page).select("div[class=content]").select("p").text()

    private fun splitTextToSentences(fullText: String): MutableList<String> =
        (fullText.split("(?<=\\.)|(?<=\\?)|(?<=!)".toRegex())).toMutableList()

}