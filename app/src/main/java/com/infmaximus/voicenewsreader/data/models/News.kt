package com.infmaximus.voicenewsreader.data.models

import java.util.*

data class News(
        val title: String = "",
        val fullText: String = "",
        val shortText: String = "",
        val articleLink: String = "",
        val date: Date? = null,
        val imageLinc: String? = ""
)